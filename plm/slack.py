import requests


class Slack:
    def __init__(self):
        self.token = "token="
        self.chanel = "channel="
        self.text = "text=" + "PLM 게시판\n"
        self.headers = {'Content-Type': 'application/json; charset=utf-8'}

    def format_message(self, board, res):
        return self.text + board + '\n' + res

    def send(self, url, token, channel, text):
        req_url = url + "?" + \
                  self.token + token + "&" + \
                  self.chanel + channel + "&" + \
                  text
        return requests.post(req_url, headers=self.headers)

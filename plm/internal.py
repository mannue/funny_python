from bs4 import BeautifulSoup
from tabulate import tabulate

class Internal:
    def __init__(self, driver):
        self.board = "내부 - https://mobilerndhub.sec.samsung.net/hub/site/selftest/statuspanel/27145698\n"
        self.driver = driver

    def parsing(self):
        _url = "/home/mannue/Downloads/Hub.html"
        self.driver.get(_url)
        _page = self.driver.page_source
        soup = BeautifulSoup(_page, 'html.parser')
        sp_body = soup.find(id='sp_body')
        thead = sp_body.find("thead")
        trs = thead.find_all('tr', {"class": "sp_head tablesorter-headerRow"})
        header_table = list()
        for tr in trs:
            for content in tr.contents:
                if content.text != "Resolve":
                    header_table.insert(int(content.attrs['data-column']), content.text.replace(" ", ""))

        tbody = sp_body.find("tbody")
        trs = tbody.find_all('tr')
        result_list = list()
        for tr in trs:
            i = 0
            body_dict = dict()
            for content in tr.contents:
                body_dict[header_table[i]] = content.text.replace(" ", "")
                i += 1
            result_list.append(body_dict)

        target = u"조성대상무"

        table_list = list()
        for result in result_list:
            if result['GL'] == target:
                table_list.append(
                    [result['TL'], result['NR'], result['3DaysResolve'], result['7DaysResolve'],
                     result['14DaysResolve']])

        return tabulate(table_list,
                       headers=[header_table[3], header_table[9], header_table[13], header_table[14], header_table[15]])
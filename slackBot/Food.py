class Food:
    def __init__(self, title, menu, text):
        self._title = title
        self._menu = menu
        self._text = text

    def title(self):
        return self._title

    def menu(self):
        return self._menu

    def text(self):
        return self._text

    def str(self):
        return ("[`" + self._title + "`]" + "[*" + self._menu + "*," + self._text + "]")

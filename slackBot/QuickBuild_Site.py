class QuickBuild_Site:
    def __init__(self):
        self.__site = dict()
        self.__site["res_deployment:dev"] = 'https://service.qb.sec.samsung.net/overview/138827'
        self.__site["res_deployment:stg"] = 'https://service.qb.sec.samsung.net/overview/139276'
        self.__site["res_deployment:prd"] = 'https://service.qb.sec.samsung.net/overview/139277'

    def get_site(self, place, stage):
        return self.__site.get(place + ":" + stage)


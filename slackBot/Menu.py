from Food import Food


class Menu:
    def make_food_list(self, restaurants):
        data_list = list()
        for restaurant in restaurants:
            data_list.append(Food(restaurant.find("span", {"class": "restaurant_titB1"}).text,
                                  restaurant.find("span", {"class": "restaurant_menu"}).text,
                                  restaurant.find("span", {"class": "restaurant_txt"}).text))
        return data_list

    def get_key(self, floor):
        last_text = floor[-1:]
        if last_text == '1':
            return 'B1'
        elif last_text == '2':
            return 'B2'
        else:
            return 'F5'

    def get_floor_data(self, soup, floor):
        div = soup.find(id=floor)
        if div is None:
            return
        try:
            fir_restaurant_list = div.find_all("div", {"class": "flr_restaurant"})
            floor_dick = dict()
            floor_dick[self.get_key(floor)] = self.make_food_list(fir_restaurant_list)
            return floor_dick
        except KeyError:
            print KeyError.message

    def get_restaurant_info(self, soup, hour):
        break_list = ['floor_1_1']
        lunch_list = ['floor_2_1', 'floor_2_2', 'floor_2_5']
        dinner_list = ['floor_3_1', 'floor_3_5']
        late_night = ['floor_4_1']

        restaurant_list = list()
        if hour < 9:
            select_list = break_list
        else:
            if hour < 14:
                select_list = lunch_list
            elif hour < 20:
                select_list = dinner_list
            else:
                select_list = late_night

        for floor in select_list:
            restaurant_list.append(self.get_floor_data(soup, floor))
        return restaurant_list

    def get_res(self, info, tag):
        str = "";
        for floors in info:
            for keys, floor in floors.items():
                if tag is not None and tag.lower() != keys.lower():
                    continue
                str += "[*" + keys + "*]" + "\n"
                for values in floor:
                    str += values.str() + "\n"
        return str

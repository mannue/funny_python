import json

import requests
from flask import Flask, request
from selenium import webdriver
from QuickBuild import QuickBuild
import threading
from LocalTime import LocalTime
from Param import Param
from MenuPlaceTable import MenuPlaceTable
from MenuService import MenuService
from Slack_Format import Slack_Format
from Menu_Format import Menu_Format
from QuickBuild_Site import QuickBuild_Site

app = Flask(__name__)
phjs_path = '/home/mannue/Downloads/phantomjs-2.1.1-linux-x86_64/bin/phantomjs'
driver = webdriver.PhantomJS(phjs_path)
place_table = MenuPlaceTable()


@app.route('/mannue/foodMenu', methods=['POST'])
def food():
    params = None
    try:
        params = Param(request.form['text']).get_params()
    except Exception as e:
        print(e.message)

    date, time = LocalTime().get_text_local_time()
    url = 'http://www.samsungwelstory.com/menu/Suwon_sec/foodcourt_R5.jsp?sDate=' + date
    service = MenuService(driver, place_table)
    food_list = service.get_food_list(url, time, params)
    if food_list is None:
        message = "Invalid parameter :: " + "".join(params)
    else:
        message = Menu_Format().get_str(food_list)

    return Slack_Format().send(message)


def booking_build(site, cmd):
    client = QuickBuild(driver)
    id = None
    password = None
    if id is not None and password is not None:
        client.quick_build_login(id, password)
        client.access_quick_build_site(site)
        client.quick_build_build(cmd)
        client.quick_build_logout()
    else:
        print("id or password is None")


@app.route('/mannue/build', methods=['POST'])
def build():
    params = None
    try:
        params = Param(request.form['text']).get_params()
    except Exception as e:
        print(e.message)

    if len(params) < 2:
        return "bad argument"
    site = QuickBuild_Site().get_site(params[0], params[1])
    slack_format = Slack_Format()
    if site is None:
        return slack_format.send("Invalid parameter")
    cmd = None
    if len(params) > 2:
        cmd = params[2]

    t = threading.Thread(target=booking_build, args=(site, cmd))
    t.start()
    return slack_format.send("Ok , I will start  : [" + " ".join(str(x) for x in params) + "] " + "Quick build")


@app.route('/mannue/oauth', methods=['GET', 'POST'])
def oauth():
    code = request.args.get('code', None)
    if not code:
        resp = app.response_class(
            status=500,
            response=json.dumps({"Error": "Looks like we're not getting code"}),
            mimetype='application/json'
        )
    else:
        url = "https://slack.com/api/oauth.access"
        client_id = ''
        client_secret = ''
        qs = {"code": code, "client_id": client_id, "client_secret": client_secret}
        r = requests.get(url, params=qs)
        if r:
            resp = app.response_class(
                status=200,
                response=json.dumps(r.json())
            )
    return resp


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)

import json

import flask


class Slack_Format:
    def send(self, message):
        res = dict()
        res['text'] = message
        res['mrkdwn'] = True
        json_value = json.dumps(res)
        response = flask.Response(json_value)
        response.headers['Content-Type'] = 'application/json; charset=utf-8'
        return response

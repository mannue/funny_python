from bs4 import BeautifulSoup
from Food import Food


class MenuService:
    def __init__(self, driver, place_table):
        self.driver = driver
        self.place_table = place_table

    def get_html(self, url):
        self.driver.get(url)
        return self.driver.page_source

    @staticmethod
    def time_range(time):
        if time < 9:
            return 0
        elif time < 14:
            return 1
        elif time < 20:
            return 2
        else:
            return 3

    def make_food_list(self, restaurants):
        data_list = list()
        for restaurant in restaurants:
            data_list.append(Food(restaurant.find("span", {"class": "restaurant_titB1"}).text,
                                  restaurant.find("span", {"class": "restaurant_menu"}).text,
                                  restaurant.find("span", {"class": "restaurant_txt"}).text))
        return data_list

    def get_element_value(self, element, tag, condition):
        if element is None:
            return None
        try:
            return element.find_all(tag, condition)
        except KeyError:
            return None

    def get_food_list(self, url, time, place):
        html = self.get_html(url)
        html_place = self.place_table.get_place(MenuService.time_range(time), place)
        if html_place is None:
            return None
        soup = BeautifulSoup(html, 'html.parser')
        floor_dick = dict()
        for floor in html_place:
            values = self.get_element_value(soup.find(id=floor), "div", {"class": "flr_restaurant"})
            floor_dick[self.place_table.get_floor(floor)] = self.make_food_list(values)
        return floor_dick
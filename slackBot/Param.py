class Param:
    def __init__(self, text):
        self.params = text.split()

    def is_invalid_param(self):
        return (len(self.params) < 1)

    def get_params(self):
        if (self.is_invalid_param()):
            return None
        return self.params
class Menu_Format:
    def get_str(self, obj):
        str = ""
        for key, values in obj.items():
            str += "[*" + key + "*]" + "\n"
            for v in values:
                str += v.str() + "\n"
        return str

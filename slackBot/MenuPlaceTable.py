class MenuPlaceTable:

    def __init__(self):
        self.__break_place = dict()
        self.__break_place['b1'] = 'floor_1_1'

        self.__lunch_place = dict()
        self.__lunch_place['b1'] = 'floor_2_1'
        self.__lunch_place['b2'] = 'floor_2_2'
        self.__lunch_place['f5'] = 'floor_2_5'

        self.__dinner_place = dict()
        self.__dinner_place['b1'] = 'floor_3_1'
        self.__dinner_place['f5'] = 'floor_3_5'

        self.__night_place = dict()
        self.__night_place['b1'] = 'floor_4_1'

        self.list = [self.__break_place, self.__lunch_place, self.__dinner_place, self.__night_place]

    def get_place(self, index, place):
        place_dict = self.list[index]
        if place is None:
            return place_dict.values()
        for x in place:
            if x in place_dict.keys():
                res = list()
                res.append(place_dict[x])
                return res
        return None

    def get_floor(self, place):
        for x in self.list:
            for key, value in x.items():
                if value == place:
                    return key

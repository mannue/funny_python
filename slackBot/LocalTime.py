import datetime


class LocalTime:
    def __init__(self):
        self.dt = datetime.datetime.now()

    def get_text_local_time(self):
        return str(self.dt.year) + (str(self.dt.month).zfill(2)) + str(self.dt.day).zfill(2), self.dt.hour

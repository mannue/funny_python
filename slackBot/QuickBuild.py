class QuickBuild:
    def __init__(self, driver):
        self.driver = driver

    def quick_build_login(self, name, passwd):
        self.driver.get('https://service.qb.sec.samsung.net/signin;jsessionid=1reonpp4shyoc16qi89nz23aky')
        self.driver.find_element_by_name("userName").send_keys(name)
        self.driver.find_element_by_name("password").send_keys(passwd)
        self.driver.find_element_by_xpath('//*[@class="maskable"]').click()

    def access_quick_build_site(self, site):
        self.driver.get(site)
        self.driver.find_element_by_xpath('//*[@class="btn run"]').click()

    def quick_build_build(self, cmd):
        if cmd is not None:
            name = self.driver.find_element_by_name(
                "editor:content:basicProperties:1:property:editor:editor:wrapper:input")
            name.clear()
            name.send_keys(cmd)
        self.driver.find_element_by_xpath('//*[@class="submits"]/button').click()

    def quick_build_logout(self):
        self.driver.implicitly_wait(3)
        self.driver.get('https://service.qb.sec.samsung.net/signout')